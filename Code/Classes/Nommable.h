/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Nommable.h
 *  @brief        Permet de nommer les composants
 *  @version      0.1
 *  @date         18 nov. 2019 10:44:19
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Nommable.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _NOMMABLE_H
#define _NOMMABLE_H

// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...

// Includes system C++
#include <string>
// Includes qt

// Includes application

// Constantes
// ex :
// const int kDaysInAWeek = 7;

// Enumerations
// ex :
// enum Couleur
// {
//     kBlue = 0,
//     kWhite,
//     kRed,
// };

// Structures
// ex:
// struct UrlTableProperties
// {
//  string name;
//  int numEntries;
// }

// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Permet de nommer les composants
 *  Description detaillee de la classe.
 */
class Nommable
{
public :
    /**
     * Constructeur
     */
    Nommable(std::string name);
    /**
     * Destructeur
     */
    virtual ~Nommable();

    std::string get_nom();


    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);

    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }

protected :
    // Attributs proteges
    std::string nom;
    // Methode protegees

private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;

    // Methodes privees
};

// Methodes publiques inline
// ex :
// inline void Nommable::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type Nommable::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _NOMMABLE_H

