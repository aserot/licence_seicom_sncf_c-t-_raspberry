/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data_Serial.cpp
 *  @brief        Permet d'acquérir les données sur le lien Serial
 *  @version      0.1
 *  @date         18 nov. 2019 10:45:55
 *
 *  Description detaillee du fichier Data_Serial.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          18 nov. 2019 10:45:55 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes application
#include "Data_Serial.h"



Data_Serial::Data_Serial(std::string name) : Bus(name), nom(name) // Hérite de l'attribut nom de la classe Bus, ainsi que de ces fonctions
{
	nom = name;


}

Data_Serial::~Data_Serial()
{
}



void Data_Serial::Data_Send()
{
	int nBitTrame ,nBit ,BitTotaux = 0 ,BitBuf = 0;

	int cport_nr = 22;   //22 correspond à ttyAMA0 || Voir à la fin pour les informations sur les nombres associés au port
	int bdrate = 115200; //Vitesse de transmission entre les deux équipements
	char mode[] = {'8','N','1',0}; // Mode 8 Bits ; pas de parité ; 1 bit de stop ; 0 pour pas de control de flux.

	unsigned char buf[1500]; // chaque trames seront composées de 1500 octets au maximum
	unsigned char Data[774*1500]; // Les fichiers produits par etrain message 1,10 Mo ( soit 1 155 072 octers ) , on prend donc 774 trames de 1500 octets chacunes ( soit 1 161 000 octets), donc légèrement plus en gage de sécurité.
	
	bool Error = false;
	

        if(RS232_OpenComport(cport_nr, bdrate, mode, 0)) // Renvoi un 1 en cas d'erreur
        {
                printf("Can not open comport\n"); // Le port n'est pas disponible ou il n'est pas bien paramétré
                Error = true; // Voir pour une possible remonté d'erreur
        }
	else{



	while(1){
	RS232_enableDTR(cport_nr); // DTR Data Terminal Ready, non utilisé pour l'instant à cause d'un câble manquant.
	
	/********************************************/
	/**Fonction De récupération des trames*******/
	/********************************************/


	for(int i = 0; i < 773 ; i++){ // Boucle qui correspond aux 773 trames composées des 1500 octets ( au maximum);

	RS232_enableRTS(cport_nr);	// Ready To Send à utiliser avec le bon câble
	std::cout <<"Tours : " << i << std::endl; // Renvoi le numéro de la trame
	//while(n != 11){ 						  // Condition d'envoi à revoir
	nBitTrame = RS232_PollComport(cport_nr, buf, 1500);//Lecture des données reçues et assignation dans buf en passant par le port serial ttyAMA0, renvoit le nombre d'octets reçues ( n )
	std::cout << "Bloqued " << std::endl; //Lié à la condition, surtout indicatif
        //std::cout <<  n;	//Donnée indicatif
	usleep(100000); // Notion de Durée à revoir, Ajout d'un timer ? où timer deja présent avec les pins
	//}
	
	RS232_disableRTS(cport_nr); // à voir avec le bon câble
	//printf("Nombre d'octets :",n,"Nombre de Tours",i); //  I
	std::cout << "Nombre d'octets ";					 //  N
	std::cout <<  nBitTrame;									 //  D
	std::cout << "Nombres de Tours";					 //  I
	std::cout << i << std::endl;					     //  CATIF
	usleep(1000000); // Notion de temps à revoir

	if( n > 0){ 
	buf[n] = 0; // Conditions pour placer un 0 à la fin du string
	}
	
	for(nBit = BitTotaux; nBit < ( BitTotaux + nBitTrame )  ; nBit ++){ //Assignation des données de la trames dans un trame global
	
	Data[nBit] = buf[BitBuf];  // Affecte a la case du tableau en cours de Data, les données de 0 à n ( nombre d'octets reçus )
	BitBuf++; 		//Bit du buffer incrémentation
	
	}

	BitBuf=0;		//Bit du buffer
	BitTotaux = BitTotaux + nBitTrame; // BifTotaux
	nBitTrame = 0;	//Nombre de bit de la trame


	}

	
	
       // printf((char *)buf);
	//printf(" ||| ");
	//printf((char *)Data);
	std::cout << " Nombre de bits réels " << nBit <<  " Nombre de bits théoriques " << BitTotaux << std::endl;	 // On affiche le nombre "d'octets" présent
		//printf("Transfo : ", Data[1][0]);
        //printf("Transfo : ", Data[2][0]);
        //printf("Transfo : ", Data[3][0]);
	std::ofstream fichier("Test.txt", std::ios::app); // On crée le fichier Test.txt dans lequel se trouve les informations
 	fichier<<Data; //On écrit les données dans le fichier Test.txt
	fichier.close(); // On ferme le fichier
	}

	usleep(100000);
    
	}

RS232_CloseComport(cport_nr);

}

/*std::string Data_Serial::DataParsing() : Bus()
{




}



/*******************************************************************************************************/
/*********************Données à garder, remettre en ordre dans un autre fichier*************************/
/*******************************************************************************************************/

   // usleep(1000000);
//}
/************************************************************************/
/* Essai de l'envoit de message avant l'ajout de la classe rs232        */
/************************************************************************/

	/*//int serial_port = get_Serialport();
	std::cout << serial_port << std::endl ;
	//lockUnLock();
	char path[] = "/dev/ttyUSB0";
	int fd = open(path, O_WRONLY | O_CREAT);
    // Acquire non-blocking exclusive lock
	if(flock(fd, LOCK_EX | LOCK_NB) == -1) 

	{
		throw std::runtime_error("Serial port with file descriptor " + 
			std::to_string(fd) + " is already locked by another process.");
	}
	else
	{
		unsigned char msg[] = { 'H','e','l','l','o',' ','!','!' };
		//cout << serial_port << endl;
		write (fd, msg, sizeof(msg));


		// Allocate memory for read buffer, set size according to your needs
		char read_buf [256];
		memset(&read_buf, '\0', sizeof(read_buf));

		// Read bytes. The behaviour of read() (e.g. does it block?,
		// how long does it block for?) depends on the configuration
		// settings above, specifically VMIN and VTIME
		int num_bytes = read(serial_port, &read_buf, sizeof(read_buf));

		// n is the number of bytes read. n may be 0 if no bytes were received, and can also be -1 to signal an error.
		if (num_bytes < 0) {
			printf("Error reading: %s", strerror(errno));
		}
		else{
		// Here we assume we received ASCII data, but you might be sending raw bytes (in that case, don't try and
		// print it to the screen like this!)
			printf("Read %i bytes. Received message: %s", num_bytes, read_buf);
		}

		close(serial_port);
	}*/




/*******************************************************************************************************/



/*************************************************************************************/
/*	Fonction pour passer directement via les broches, les broches RX et TX soit 8et10*/
/*  sont déjà reconnu comme une sortie ttyS0										 */
/*************************************************************************************/
/*void TestGPIO()
{

}*/
// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Data_Serial::NomMethode(Type parametre)
// {
// }

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale Data_Serial" << endl;
//    Data_Serial *data_serial = new Data_Serial();
//    // Lancement de l'activité principale
//    return 0;
// }



/*************************************************************************************/
/*	Ancienne fonction D'initialisation du port Serial, où l'on manipule les registres*/
/*  Directements. La nouvelle classe rs232 ajoutée facilite l'utilisation de ceux-ci */
/*************************************************************************************/

/*void Data_Serial::InitSerial()
{
	set_i(0);
	set_cport_nr(16);
	set_bdrate(9600);
	mode = 
	set_mode('8','N','1',0);*/

	//Initialisation du port Com



/*// Create new termios struc, we call it 'tty' for convention	
	struct termios tty;
int serial_port = open("/dev/ttyUSB0", O_RDWR); // On déclare serial_port qui va permettre de vérifier la disponibilité de la liaison
												 // Paramètre disponible 
												 //O_RDONLY Lecture seulement
												 //O_WRONLY Ecriture seulement
												 //O_RDWR Ecriture et lecture
// Check for errors
if (serial_port < 0) {	 // On regarde si celle-ci est disponible
	printf("Error %i from open: %s\n", errno, strerror(errno));
}

else
{

	memset(&tty, 0, sizeof tty);

	// Read in existing settings, and handle any error
	if (tcgetattr(serial_port, &tty) != 0 )
	{
		printf("Error %i FROM tcgetattr: %s\n", errno, strerror(errno));

	}

	else
	{

	tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	//tty.c_cflag |= PARENB;  // Set parity bit, enabling parity

	tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	//tty.c_cflag |= CSTOPB;  // Set stop field, two stop bits used in communication

	//tty.c_cflag |= CS5; // 5 bits per byte
	//tty.c_cflag |= CS6; // 6 bits per byte
	//tty.c_cflag |= CS7; // 7 bits per byte
	tty.c_cflag |= CS8; // 8 bits per byte (most common)

	tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	//tty.c_cflag |= CRTSCTS;  // Enable RTS/CTS hardware flow control

	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)
	tty.c_lflag &= ~ICANON; // not receiving line-by-line

	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo

	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	
	tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	cfsetispeed(&tty, B115200); // set in baud rate to 9600 maybe different in the MIM card
	cfsetospeed(&tty, B115200); // set out baud rate to 9600 maybe different in the MIM card

		// Save tty settings, also checking for error
	if (tcsetattr(serial_port, TCSANOW, &tty) != 0) 
	{

		printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
	}

}
}

return serial_port;
}*/