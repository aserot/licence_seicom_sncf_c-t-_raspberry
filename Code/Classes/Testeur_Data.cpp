/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Testeur_Data.cpp
 *  @brief        Fonction principal du programme
 *  @version      0.1
 *  @date         18 nov. 2019 10:46:44
 *
 *  Description detaillee du fichier Testeur_Data.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          18 nov. 2019 10:46:44 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++

// Includes qt

// Includes application
#include "Testeur_Data.h"

/**
 * Constructeur
 */
Testeur_Data::Testeur_Data()
{
	int etat;
	bool Serial,
	 DataNotSaved,
	  DataSend,
	   DataNotSend,
	    Received,
	     DataCount,
	      Check,
	       Saved,
	        BusyMemory;

}

/**
 * Destructeur
 */
Testeur_Data::~Testeur_Data()
{
}

// Methodes publiques

// Methodes protegees
etat Testeur_Data::SwitchCase(etat,Serial,DataNotSaved,DataSend,DataNotSend,Received,DataCount,Check,Saved,BusyMemory){
 		
 		switch(etat){

 			case(1):
 				if ( Serial == false){
 					etat = 2;
 				}
 				else if(/*timer Front Montant true*/){
 					etat = 3;
 				}

 			case(2):
 				if ( Serial == true){
 					etat = 1;
 				}

 		}
}
// Methodes privees






// Programme principal

 int main()
 {
 	while(0)
 	{
 		switch(etat){

 			case(1):
 		}
 	}

    // Construction de l'instance de la classe principale
    //std::cout << "Construction de l'instance de la classe principale Testeur_Data" << std::endl;
    //Testeur_Data *testeur_data = new Testeur_Data();
    // Lancement de l'activité principale

    Data_Serial *data_serial = new Data_Serial("Liaison Serial");
   //data_serial->Data_Serial::InitSerial();
    //int serial_port = data_serial->Data_Serial::get_Serialport();


    data_serial->Data_Serial::Data_Send();
    std::cout << data_serial->get_nom() << std::endl;

    return 0;
 }

