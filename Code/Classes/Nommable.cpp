/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Nommable.cpp
 *  @brief        Permet de nommer les composants
 *  @version      0.1
 *  @date         18 nov. 2019 10:44:19
 *
 *  Description detaillee du fichier Nommable.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          18 nov. 2019 10:44:19 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "Nommable.h"

/**
 * Constructeur
 */
Nommable::Nommable(std::string name) : nom(name)
{
	  nom=name;
}

/**
 * Destructeur
 */
Nommable::~Nommable(){};



/**
 * Assesseur du nom
 */

std::string Nommable::get_nom(){
    return nom;
};

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Nommable::NomMethode(Type parametre)
// {
// }

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale Nommable" << endl;
//    Nommable *nommable = new Nommable();
//    // Lancement de l'activité principale
//    return 0;
// }

