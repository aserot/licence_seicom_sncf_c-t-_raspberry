/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data_Serial.h
 *  @brief        Permet d'acquérir les données sur le lien Serial
 *  @version      0.1
 *  @date         18 nov. 2019 10:45:55
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Data_Serial.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _DATA_SERIAL_H
#define _DATA_SERIAL_H

// Includes system C
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Includes system C++
#include <string>
#include <iostream>
#include <fstream>
#include <cstdio>

// Includes qt

// Includes application
#include "Bus.h"
#include "rs232.h"

// Include Linux headers
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>




class Data_Serial : public Bus
{
public :

    /**
     * Constructeur
     */
    Data_Serial(std::string);    
    /**
     * Destructeur
     */
    ~Data_Serial();



    // Methodes publiques de la classe
    //void InitSerial();
    void Data_Send();
    //void TestGPIO();
    
    int get_Serialport(void) const { return serial_port; };
protected :
    // Attributs proteges
    // Methode protegees

private :
    // Attributs prives
	std::string nom;
    int serial_port;

    /*int i;
	int cport_nr;
	int bdrate;
    char mode;*/

    // Methodes privees
};



#endif  // _DATA_SERIAL_H



/***********************************************************************************/
/******************** Test précédent en cas de Besoin ******************************/
/***********************************************************************************/

    /*int get_i(){
    	return i;
    }
    void set_i(int i_){
    	i=i_;
    }

    int get_cport_nr(){
    	return cport_nr;
    }
    void set_cport_nr(int cport_nr_){
    	cport_nr = cport_nr_;
    }

    int get_bdrate(){
    	return bdrate;
    }
    void set_bdrate(int bdrate_){
    	bdrate = bdrate_;
    }

    char* get_mode(){
    	return mode;
    }
    void set_mode(char* Param){
     	mode = Param;
    }*/

