/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Testeur_Data.h
 *  @brief        Fonction principal du programme
 *  @version      0.1
 *  @date         18 nov. 2019 10:46:44
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Testeur_Data.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _TESTEUR_DATA_H
#define _TESTEUR_DATA_H

// Includes system C

// Includes system C++

// Includes qt

// Includes application
#include "Data_Serial.h"


/** @brief Fonction principal du programme
 *  Description detaillee de la classe.
 */
class Testeur_Data
{
public :

    /**
     * Constructeur
     */
    Testeur_Data();

    /**
     * Destructeur
     */
    ~Testeur_Data();

    int SwitchCase(int,bool,bool,bool,bool,bool,bool,bool,bool,bool);

protected :

    // Attributs proteges

    // Methode protegees

private :

    // Attributs prives

    // Methodes privees
};

#endif  // _TESTEUR_DATA_H

