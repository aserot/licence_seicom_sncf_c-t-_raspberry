/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data.h
 *  @brief        Classe mère abstraite des différentes classes Data
 *  @version      0.1
 *  @date         18 nov. 2019 10:45:06
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Data.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _DATA_H
#define _DATA_H


// Includes application
#include "Nommable.h"


/** @brief Classe mère abstraite des différentes classes Data
 *  Description detaillee de la classe.
 */
class Bus : public Nommable // la classe Bus hérite de Nommable, permettant un nommage des éléments
{

public :

    /* Constructeur */
    Bus(std::string);
    
    /* Destructeur */
    virtual ~Bus(); //Définition en virtual puisque la classe, étant abstraite, aucun constructeur ne sera jamais créé



    //void Data_Acquisition();
    //void Data_Realise();


    std::string get_Data(){return Data_;}; // Assesseur des données

    void set_Data(std::string Data){ Data_ = Data;}; // Assesseur des données 

    std::string toString(); //Affichage 

    //virtual std::string DataParsing();


protected :

    // Attributs proteges
    std::string Data_;
    std::string nom;
    // Methode protegees

private :
    // Attributs prives

    // Methodes privees

};


#endif  // _DATA_H

