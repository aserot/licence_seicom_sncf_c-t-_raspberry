
/*
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data_SerialTest.cpp
 *  Classe        Data_Serial
 *  @note         Implementation de la classe en charge des tests unitaires
 */

// Includes system C

// Includes system C++
#include <cppunit/CompilerOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>

// Includes qt

// Includes application
#include "Data_SerialTest.h"

// Registers the fixture into the 'registry'
CPPUNIT_TEST_SUITE_REGISTRATION(Data_SerialTest);

// Exemple d'assertions possibles
// # CPPUNIT_ASSERT(condition)
// Assertions that a condition is true.
// # CPPUNIT_ASSERT_MESSAGE(message, condition)
// Assertion with a user specified message.
// # CPPUNIT_FAIL(message)
// Fails with the specified message
// # CPPUNIT_ASSERT_EQUAL(expected, actual)
// Asserts that two values are equals.
// # CPPUNIT_ASSERT_EQUAL_MESSAGE(message, expected, actual)
// Asserts that two values are equals, provides additional message on failure
// # CPPUNIT_ASSERT_DOUBLES_EQUAL(expected, actual, delta)

// setUp() to initialize the variables you will use for test
void Data_SerialTest::setUp()
{
}

// tearDown() to release any permanent resources you allocated in setUp()
void Data_SerialTest::tearDown()
{
}

// Suite des tests unitaires

void Data_SerialTest::testConstructor()
{
    // Construction de l'instance de classe a tester
	Data_Serial *data_serial = new Data_Serial("Liaison Serial");
	CPPUNIT_ASSERT(data_serial != NULL);
	delete data_serial;
}

void Data_SerialTest::testUnitaire1()
{
    // Construction de l'instance de classe a tester
	std::cout << "" << std::endl;
	Data_Serial *data_serial = new Data_Serial("Liaison Serial");
	std::cout << data_serial->get_nom() << std::endl;
	//data_serial->Data_Serial::InitSerial();
    data_serial->Data_Serial::Data_Send(); 
	/*int cport_nr = data_serial->get_cport_nr();
	int bdrate = data_serial->get_bdrate();
	char* mode = data_serial->get_mode();*/

	//std::cout << mode << std::endl;

	/*if(RS232_OpenComport(cport_nr, bdrate, mode, 0))
	{
		data_serial = NULL;
	}
	printf("Port Open");*/



	CPPUNIT_ASSERT(data_serial != NULL);
    // Test unitaire d'une methode publique de la classe
    // Utilisation des macros CPPUNIT_ASSERT, CPPUNIT_ASSERT_EQUAL, etc.
	delete data_serial;
}

void Data_SerialTest::testUnitaire2()
{
    // Construction de l'instance de classe a tester
	std::cout << "" << std::endl;

	Data_Serial *data_serial = new Data_Serial("Liaison Serial");
	std::cout << data_serial->get_nom() << std::endl;            //Récupère la valeur du port serial
	//data_serial->Data_Serial::InitSerial(); 
    data_serial->Data_Serial::Data_Send();                  //En fonction de la valeur récupérer, effectuer la récupération de données sur le bon port.

    CPPUNIT_ASSERT(data_serial != NULL);

    // Test unitaire d'une methode publique de la classe

    // Utilisation des macros CPPUNIT_ASSERT, CPPUNIT_ASSERT_EQUAL, etc.
    delete data_serial;
}

// the main method
int main(int argc, char* argv[])
{
    // informs test-listener about testresults
	CPPUNIT_NS::TestResult testresult;

    // register listener for collecting the test-results
	CPPUNIT_NS::TestResultCollector collectedresults;
	testresult.addListener(&collectedresults);

    // register listener for per-test progress output
	CPPUNIT_NS::BriefTestProgressListener progress;
	testresult.addListener(&progress);

    // insert test-suite at test-runner by registry
	CPPUNIT_NS::TestRunner testrunner;
	testrunner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
	testrunner.run(testresult);

    // output results in compiler-format
	CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
	compileroutputter.write();

    // for hudson
	std::ofstream file( "Data_Serial-cppunit-report.xml" );
	CPPUNIT_NS::XmlOutputter xmloutputter(&collectedresults, file);
	xmloutputter.write();
	file.close();

    // return 0 if tests were successful
	return collectedresults.wasSuccessful() ? 0 : 1;
}

