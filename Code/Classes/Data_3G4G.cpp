/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data_3G-4G.cpp
 *  @brief        Permet d'envoyer les informations par 3G
 *  @version      0.1
 *  @date         18 nov. 2019 10:46:23
 *
 *  Description detaillee du fichier Data_3G-4G.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          18 nov. 2019 10:46:23 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "Data_3G4G.h"

/**
 * Constructeur
 */
Data_3G4G::Data_3G4G()
{
}

/**
 * Destructeur
 */
Data_3G4G::~Data_3G4G()
{
}

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Data_3G-4G::NomMethode(Type parametre)
// {
// }

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale Data_3G-4G" << endl;
//    Data_3G-4G *data_3g-4g = new Data_3G-4G();
//    // Lancement de l'activité principale
//    return 0;
// }

