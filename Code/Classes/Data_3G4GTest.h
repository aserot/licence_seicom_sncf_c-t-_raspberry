
/*
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data_3G-4GTest.h
 *  Classe        Data_3G-4G
 *  @note         Classe en charge des tests unitaires
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _DATA_3G4GTEST_H
#define _DATA_3G4GTEST_H

// Includes system C

// Includes system C++
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Includes qt

// Includes application
#include "Data_3G4G.h"

class Data_3G4GTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(Data_3G4GTest);
    CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST(testUnitaire1);
    CPPUNIT_TEST_SUITE_END();

public :
    void setUp();
    void tearDown();

    void testConstructor();
    void testUnitaire1();

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives

    // Methodes privees
};

#endif  // _DATA_3G-4GTEST_H


