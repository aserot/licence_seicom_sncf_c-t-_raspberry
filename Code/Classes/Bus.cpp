/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data.cpp
 *  @brief        Classe mère abstraite des différentes classes Data
 *  @version      0.1
 *  @date         18 nov. 2019 10:45:06
 *
 *  Description detaillee du fichier Data.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          18 nov. 2019 10:45:06 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


// Includes application
#include "Bus.h"


/* Constructeur */
Bus::Bus(std::string name) : Nommable(name), nom(name)
{
	 nom = name;
}

/* Destructeur */
Bus::~Bus()
{
}

/* Fonction Affichage */
std::string Bus::toString(){ // Encore à définir
std::string res;
res = "Hello World";
return res;
}

