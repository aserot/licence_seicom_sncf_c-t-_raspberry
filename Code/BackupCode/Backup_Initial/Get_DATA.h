/**
 *  Copyright (C) 2020  aserot  (serot.arthur@sncf.fr)
 *  @file         Get_DATA.h
 *  @brief        Classe Get_DATA du logiciel SQIZ_DATA
 *  @version      0.1
 *  @date         22 Octobre 2020
 *
 */

#ifndef _Get_DATA_H
#define _Get_DATA_H

/* Include system C */
//#include <stdint.h>


/* Includes system C++ */





/* Includes Application */



// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Classe Get_DATA du logiciel SQIZ_DATA
 *  Description detaillee de la classe.
 */

class Get_DATA 
{

	private :

	public :

    /**
     * Constructeur
     */

	Get_DATA();

	/**
     * Destructeur
     */

	~Get_DATA();

	protected :


};

#endif  // _Get_DATA_H