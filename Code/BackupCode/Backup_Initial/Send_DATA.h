/**
 *  Copyright (C) 2020  aserot  (serot.arthur@sncf.fr)
 *  @file         Send_DATA.h
 *  @brief        Classe Send_DATA du logiciel SQIZ_DATA
 *  @version      0.1
 *  @date         22 Octobre 2020
 *
 */

#ifndef _Send_DATA_H
#define _Send_DATA_H

/* Include system C */
//#include <stdint.h>


/* Includes system C++ */





/* Includes Application */



// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Classe Send_DATA du logiciel SQIZ_DATA
 *  Description detaillee de la classe.
 */

class Send_DATA 
{

	private :

	public :

    /**
     * Constructeur
     */

	Send_DATA();

	/**
     * Destructeur
     */

	~Send_DATA();

	protected :


};

#endif  // _Send_DATA_H