/**
 *  Copyright (C) 2020  aserot  (serot.arthur@sncf.fr)
 *  @file         main.h
 *  @brief        Classe main du logiciel SQIZ_DATA
 *  @version      0.1
 *  @date         22 Octobre 2020
 *
 */

#ifndef _main_H
#define _main_H

/* Include system C */
//#include <stdint.h>


/* Includes system C++ */





/* Includes Application */
#include "Send_DATA.h"
#include "Get_DATA.h"


// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Classe main du logiciel SQIZ_DATA
 *  Description detaillee de la classe.
 */

class main 
{

	private :

	public :

    /**
     * Constructeur
     */

	main();

	/**
     * Destructeur
     */

	~main();

	protected :


};


#endif  // _main_H