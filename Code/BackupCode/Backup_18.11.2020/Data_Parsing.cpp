/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data_Parsing.cpp
 *  @brief        Permet de Trier les Données Reçu avant de les renvoyer
 *  @version      0.1
 *  @date         24 oct. 2019 09:23:50
 *
 *  Description detaillee du fichier Data_Parsing.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          24 oct. 2019 09:23:50 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "Data_Parsing.h"

/**
 * Constructeur
 */
Data_Parsing::Data_Parsing()
{
}

/**
 * Destructeur
 */
Data_Parsing::~Data_Parsing()
{
}

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Data_Parsing::NomMethode(Type parametre)
// {
// }

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale Data_Parsing" << endl;
//    Data_Parsing *data_parsing = new Data_Parsing();
//    // Lancement de l'activité principale
//    return 0;
// }

