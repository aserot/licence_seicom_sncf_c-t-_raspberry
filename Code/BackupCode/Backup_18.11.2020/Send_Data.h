/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Send_Data.h
 *  @brief        Envoit des Données depuis la carte RaspBerry Pi vers la base de données distante
 *  @version      0.1
 *  @date         22 oct. 2019 15:15:07
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Send_Data.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _SEND_DATA_H
#define _SEND_DATA_H

// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...

// Includes system C++
#include <string>

// Includes qt

// Includes application

// Constantes
// ex :
// const int kDaysInAWeek = 7;

// Enumerations
// ex :
// enum Couleur
// {
//     kBlue = 0,
//     kWhite,
//     kRed,
// };

// Structures
// ex:
// struct UrlTableProperties
// {
//  string name;
//  int numEntries;
// }
struct Data
{
	std::string Data;
	std::string Time;
	int ms;
	std::string Code;
	std::string Mnemo;
	std::string Element;
	std::string Equipements;
	std::string Reflog;
	std::string Label;
	std::string Context;				
};

// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Envoit des Données depuis la carte RaspBerry Pi vers la base de données distante
 *  Description detaillee de la classe.
 */
class Send_Data
{
public :
    /**
     * Constructeur
     */
    Send_Data();
    /**
     * Destructeur
     */
    ~Send_Data();

    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);
    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;			
	bool DataReady_;

    // Methodes privees
};

// Methodes publiques inline
// ex :
// inline void Send_Data::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type Send_Data::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _SEND_DATA_H

