/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Data_Parsing.h
 *  @brief        Permet de Trier les Données Reçu avant de les renvoyer
 *  @version      0.1
 *  @date         24 oct. 2019 09:23:50
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Data_Parsing.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _DATA_PARSING_H
#define _DATA_PARSING_H

// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...

// Includes system C++

// Includes qt

// Includes application

// Constantes
// ex :
// const int kDaysInAWeek = 7;

// Enumerations
// ex :
// enum Couleur
// {
//     kBlue = 0,
//     kWhite,
//     kRed,
// };

// Structures
// ex:
// struct UrlTableProperties
// {
//  string name;
//  int numEntries;
// }

// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Permet de Trier les Données Reçu avant de les renvoyer
 *  Description detaillee de la classe.
 */
class Data_Parsing
{
public :
    /**
     * Constructeur
     */
    Data_Parsing();
    /**
     * Destructeur
     */
    ~Data_Parsing();

    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);

    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;

    // Methodes privees
};

// Methodes publiques inline
// ex :
// inline void Data_Parsing::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type Data_Parsing::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _DATA_PARSING_H

