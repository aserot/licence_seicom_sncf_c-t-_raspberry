/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Principal.cpp
 *  @brief        Classe Principal du programme regroupant les sous parties
 *  @version      0.1
 *  @date         23 oct. 2019 09:49:01
 *
 *  Description detaillee du fichier Principal.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          23 oct. 2019 09:49:01 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
#include <iostream>
 using namespace std;

// Includes qt

// Includes application
#include "Principal.h"

/**
 * Constructeur
 */
Principal::Principal()
{
}

/**
 * Destructeur
 */
Principal::~Principal()
{
}

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Principal::NomMethode(Type parametre)
// {
// }

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
 int main(int argc, char *argv[])
 {
    // Construction de l'instance de la classe principale
    cout << "Construction de l'instance de la classe principale Principal" << endl;
    Principal *principal = new Principal();
    Get_Data *get_data = new Get_Data();
    //get_data->Get_Data::InitSerial();

    //get_data->Get_Data::getSerial_port_();
    //get_data->Get_Data::Acquisition( get_data->Get_Data::getSerial_port_() ); //On réalise l'acquisition en récupérant la valeur du port serial
    get_data->Get_Data::Acquisition( get_data->Get_Data::InitSerial() ); //On réalise l'acquisition en récupérant la valeur du port serial
    // Lancement de l'activité principale
    return 0;
 }

