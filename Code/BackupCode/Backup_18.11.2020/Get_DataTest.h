
/*
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Get_DataTest.h
 *  Classe        Get_Data
 *  @note         Classe en charge des tests unitaires
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _GET_DATATEST_H
#define _GET_DATATEST_H

// Includes system C

// Includes system C++
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Includes qt

// Includes application
#include "Get_Data.h"

class Get_DataTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(Get_DataTest);
    CPPUNIT_TEST(testConstructor);
    CPPUNIT_TEST(testUnitaire1);
    CPPUNIT_TEST(testUnitaire2);
    CPPUNIT_TEST_SUITE_END();

public :
    void setUp();
    void tearDown();

    void testConstructor();
    void testUnitaire1();
    void testUnitaire2();

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives

    // Methodes privees
};

#endif  // _GET_DATATEST_H


