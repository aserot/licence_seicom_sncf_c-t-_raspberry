/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Send_Data.cpp
 *  @brief        Envoit des Données depuis la carte RaspBerry Pi vers la base de données distante
 *  @version      0.1
 *  @date         22 oct. 2019 15:15:07
 *
 *  Description detaillee du fichier Send_Data.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          22 oct. 2019 15:15:07 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
// using namespace std;

// Includes qt

// Includes application
#include "Send_Data.h"

/**
 * Constructeur
 */
Send_Data::Send_Data()
{
}

/**
 * Destructeur
 */
Send_Data::~Send_Data()
{
}

// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Send_Data::NomMethode(Type parametre)
// {
// }

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale Send_Data" << endl;
//    Send_Data *send_data = new Send_Data();
//    // Lancement de l'activité principale
//    return 0;
// }

