/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Get_Data.cpp
 *  @brief        Acquisition des Données depuis la carte MIM via la carte RaspBerry Pi
 *  @version      0.1
 *  @date         22 oct. 2019 15:14:42
 *
 *  Description detaillee du fichier Get_Data.cpp
 *  Fabrication   gcc (Debian 9.2.1-4) 9.2.1 20190821 
 *  @todo         Liste des choses restant a faire.
 *  @bug          22 oct. 2019 15:14:42 - Aucun pour l'instant
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

// Includes system C

// Includes system C++
// A décommenter si besoin cout, cin, ...
// #include <iostream>
using namespace std;

// Includes qt

// Includes application
#include "Get_Data.h"

/**
 * Constructeur
 */
Get_Data::Get_Data()
{
}

/**
 * Destructeur
 */
Get_Data::~Get_Data()
{
}

int Get_Data::InitSerial()
{

// Create new termios struc, we call it 'tty' for convention	
struct termios tty;
int serial_port = open("/dev/ttyUSB0", O_RDWR); // On déclare serial_port qui va permettre de vérifier la disponibilité de la liaison
												 // Paramètre disponible 
												 //O_RDONLY Lecture seulement
												 //O_WRONLY Ecriture seulement
												 //O_RDWR Ecriture et lecture
// Check for errors
if (serial_port < 0) {	 // On regarde si celle-ci est disponible
	printf("Error %i from open: %s\n", errno, strerror(errno));
}

else
{

	memset(&tty, 0, sizeof tty);

	// Read in existing settings, and handle any error
	if (tcgetattr(serial_port, &tty) != 0 )
	{
		printf("Error %i FROM tcgetattr: %s\n", errno, strerror(errno));

	}

	else
	{

	tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)
	//tty.c_cflag |= PARENB;  // Set parity bit, enabling parity

	tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)
	//tty.c_cflag |= CSTOPB;  // Set stop field, two stop bits used in communication

	//tty.c_cflag |= CS5; // 5 bits per byte
	//tty.c_cflag |= CS6; // 6 bits per byte
	//tty.c_cflag |= CS7; // 7 bits per byte
	tty.c_cflag |= CS8; // 8 bits per byte (most common)

	tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)
	//tty.c_cflag |= CRTSCTS;  // Enable RTS/CTS hardware flow control

	tty.c_cflag |= CREAD | CLOCAL; // Turn on READ & ignore ctrl lines (CLOCAL = 1)
	tty.c_lflag &= ~ICANON; // not receiving line-by-line

	tty.c_lflag &= ~ECHO; // Disable echo
	tty.c_lflag &= ~ECHOE; // Disable erasure
	tty.c_lflag &= ~ECHONL; // Disable new-line echo

	tty.c_lflag &= ~ISIG; // Disable interpretation of INTR, QUIT and SUSP
	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
	tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL); // Disable any special handling of received bytes

	tty.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes (e.g. newline chars)
	tty.c_oflag &= ~ONLCR; // Prevent conversion of newline to carriage return/line feed
	
	tty.c_cc[VTIME] = 10;    // Wait for up to 1s (10 deciseconds), returning as soon as any data is received.
	tty.c_cc[VMIN] = 0;

	cfsetispeed(&tty, B9600); // set in baud rate to 9600 maybe different in the MIM card
	cfsetospeed(&tty, B9600); // set out baud rate to 9600 maybe different in the MIM card

		// Save tty settings, also checking for error
	if (tcsetattr(serial_port, TCSANOW, &tty) != 0) 
	{

		printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
	}

}
}

return serial_port;
}

void Get_Data::Acquisition(int serial_port)
{
	//lockUnLock();
	char path[] = "test.txt";
	int fd = open(path, O_WRONLY | O_CREAT);
    // Acquire non-blocking exclusive lock
	if(flock(fd, LOCK_EX | LOCK_NB) == -1) 

	{
		throw std::runtime_error("Serial port with file descriptor " + 
			std::to_string(fd) + " is already locked by another process.");
	}
	else
	{
		unsigned char msg[] = { 'H','e','l','l','o',' ','!' };
		//cout << serial_port << endl;
		write (serial_port, "Hello !", sizeof(msg));


		// Allocate memory for read buffer, set size according to your needs
		char read_buf [256];
		memset(&read_buf, '\0', sizeof(read_buf));

		// Read bytes. The behaviour of read() (e.g. does it block?,
		// how long does it block for?) depends on the configuration
		// settings above, specifically VMIN and VTIME
		int num_bytes = read(serial_port, &read_buf, sizeof(read_buf));

		// n is the number of bytes read. n may be 0 if no bytes were received, and can also be -1 to signal an error.
		if (num_bytes < 0) {
			printf("Error reading: %s", strerror(errno));
		}
		else{
		// Here we assume we received ASCII data, but you might be sending raw bytes (in that case, don't try and
		// print it to the screen like this!)
			printf("Read %i bytes. Received message: %s", num_bytes, read_buf);
		}

		close(serial_port);
	}



}




// Methodes publiques
// ex :
/**
 * Exemple de description d'une methode
 *
 * @param un_parametre  Description d'un parametre
 * @return              Description du retour
 */
// ReturnType Get_Data::NomMethode(Type parametre)
// {
// }

// Methodes protegees

// Methodes privees

// Programme principal
// Si c'est la classe principale du projet, on place une fonction main()
// Dans ce cas, on peut supprimer les fichiers de tests unitaires
// ex :
// int main(int argc, char *argv[])
// {
//    // Construction de l'instance de la classe principale
//    cout << "Construction de l'instance de la classe principale Get_Data" << endl;
//    Get_Data *get_data = new Get_Data();
//    // Lancement de l'activité principale
//    return 0;
// }

