/**
 *  Copyright (C) 2019  arthur.serot  (arthur.serot@sncf.fr)
 *  @file         Get_Data.h
 *  @brief        Acquisition des Données depuis la carte MIM via la carte RaspBerry Pi
 *  @version      0.1
 *  @date         22 oct. 2019 15:14:42
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                Get_Data.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _GET_DATA_H
#define _GET_DATA_H

// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...
#include <stdio.h>
#include <string.h>
// Includes system C++
#include <string>
#include <iostream>

// Includes qt
//#include <QtCore>

// Includes application
//#include "qextserialport.h"

// Include Linux headers
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
// Constantes
// ex :
// const int kDaysInAWeek = 7;

// Enumerations
// ex :
// enum Couleur
// {
//     kBlue = 0,
//     kWhite,
//     kRed,
// };

// Structures
// ex:
// struct UrlTableProperties
// {
//  string name;
//  int numEntries;
// }

// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief Acquisition des Données depuis la carte MIM via la carte RaspBerry Pi
 *  Description detaillee de la classe.
 */
class Get_Data 
{
public :
    /**
     * Constructeur
     */
    Get_Data();
    /**
     * Destructeur
     */
    ~Get_Data();

    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);
  //  bool OpenPort(QString port_name,BaudRateType baud,DataBitsType data_len,ParityType par,StopBitsType stop,FlowType flow);
  // bool ClosePort(void);
    int InitSerial();
    void Acquisition(int);
    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }
    std::string getData_(void) const { return Data_; };
    int getSerial_port_(void) const { return serial_port; };

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;
	std::string Data_;
	int serial_port;
	//QextSerialPort* port_;
	//QByteArray buffRecept_;

    // Methodes privees
};

// Methodes publiques inline
// ex :
// inline void Get_Data::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type Get_Data::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _GET_DATA_H

